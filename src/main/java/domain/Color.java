package domain;

public enum Color {
    red("red"),
    blue("blue"),
    black("black"),
    brown("brown"),
    green("green"),
    yellow("yellow"),
    ;

    private String color;

    Color(String color) {
        setColor(color);
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Color{" +
                "color='" + color + '\'' +
                '}';
    }
}
