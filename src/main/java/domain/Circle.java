package domain;

public class Circle extends Shape {
    public double radius;

    public Circle() {
        setRadius(1.0);
    }

    public Circle(double radius) {
        setRadius(radius);
    }

    public Circle(Color color, boolean filled, double radius) {
        super(color, filled);
        setRadius(radius);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    public double getLength() {
        return Math.PI * radius;
    }

    @Override
    public String toString() {
        return "A Circle with radius " + radius + " which is a subclass of " + super.toString();
    }
}
