package domain;

import org.w3c.dom.css.Rect;

public class Rectangle extends Shape {
    public double width;
    public double length;

    Rectangle() {
        setLength(1.0);
        setWidth(1.0);
    }

    Rectangle(double length, double width) {
        setLength(length);
        setWidth(width);
    }

    Rectangle(Color color, boolean filled, double width, double length) {
        super(color, filled);
        setLength(length);
        setWidth(width);
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea() {
        return length * width;
    }

    public double getPerimeter() {
        return 2 * (length + width);
    }

    @Override
    public String toString() {
        return "A rectangle with width " + width + " and length " + length + " which is a subclass of " + super.toString();
    }
}
