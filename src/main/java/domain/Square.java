package domain;

public class Square extends Rectangle {

    public Square() {
        setLength(1.0);
        setWidth(1.0);
    }

    public Square(double side) {
        super(side, side);
    }

    public Square(Color color, boolean filled, double side) {
        super(color, filled, side, side);
    }

    public double getSide(){
        return super.getWidth();
    }

    public void setSide(double side){
        super.setLength(side);
        super.setWidth(side);
    }


    @Override
    public String toString() {
        return "A square with side " + getSide() + " which is a subclass of " + super.toString();
    }

    @Override
    public void setWidth(double width) {
        setSide(width);
    }

    @Override
    public void setLength(double length) {
        setSide(length);
    }
}
